$(function(){
    $("[data-toogle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:3000
    });

    $('#alquilar').on('show.bs.modal', function(e){
     $('#reservabtn1').removeClass('btn-success');
     $('#reservabtn1').addClass('btn-primary');
     $('#reservabtn1').prop('disabled',true);
    });

    $('#alquilar').on('shown.bs.modal', function(e){
        $('#reservabtn2').removeClass('btn-success');
        $('#reservabtn2').addClass('btn-primary');
        $('#reservabtn1').prop('disabled',true);
       });

    $('#alquilar').on('hide.bs.modal', function(e){
        $('#reservabtn1').removeClass('btn-primary');
        $('#reservabtn1').addClass('btn-success');
        $('#reservabtn1').prop('disabled',false);
    });

    $('#alquilar').on('hidden.bs.modal', function(e){
        $('#reservabtn2').removeClass('btn-primary');
        $('#reservabtn2').addClass('btn-success');
        $('#reservabtn1').prop('disabled',false);
       });


});